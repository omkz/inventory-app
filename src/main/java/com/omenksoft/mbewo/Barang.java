/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.omenksoft.mbewo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author omenk
 */
@Entity
public class Barang implements Serializable {
    
    
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;
    private String nama;
    private Integer hargaBeli;
    private Integer stok;

    public Integer getHargaBeli() {
        return hargaBeli;
    }

    public void setHargaBeli(Integer hargaBeli) {
        this.hargaBeli = hargaBeli;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getStok() {
        return stok;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }
    
    
    
    
}
